package nfree_mgouletas.svu_location;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class EditActivity extends AppCompatActivity {

    protected EditText offence_street;
    protected EditText offence_zip;
    protected EditText offence_city;
    protected EditText offence_state;
    protected Spinner offence_building;
    protected Button finish_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main_edit);
        registerViews();

        Spinner dropdown = (Spinner)findViewById(R.id.spinner_building_type);
        String[] items = new String[]{"House", "Apartment", "Trailer", "Other"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, items);
        dropdown.setAdapter(adapter);
        dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                //String selected_item = dropdown.getSelectedItem().toString();
                EditText apartmentnumber = (EditText)findViewById(R.id.apartment_number);
                EditText trailernumber = (EditText)findViewById(R.id.lot_number);
                switch (i) {
                    case 1:
                        apartmentnumber.setVisibility(View.VISIBLE);
                        trailernumber.setVisibility(View.GONE);
                        break;
                    case 2:
                        trailernumber.setVisibility(View.VISIBLE);
                        apartmentnumber.setVisibility(View.GONE);
                        break;
                    default:
                        apartmentnumber.setVisibility(View.GONE);
                        trailernumber.setVisibility(View.GONE);
                        break;
                }

            }


            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        /*Button button_finish = (Button) findViewById(R.id.button_finish);
        button_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_OK, getIntent());
                finish();
            }
        });*/

    }

    private void registerViews() {
        offence_street = (EditText) findViewById(R.id.offence_street_add);
        offence_city = (EditText) findViewById(R.id.editText2);
        offence_state = (EditText) findViewById(R.id.offence_state);
        offence_zip = (EditText) findViewById(R.id.editText);
        offence_building = (Spinner) findViewById(R.id.spinner_building_type);

        // TextWatcher would let us check validation error on the fly
        offence_street.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                Validation.hasText(offence_street);
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        offence_city.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                Validation.hasText(offence_city);
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        offence_state.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                Validation.hasText(offence_state);
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        offence_zip.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                Validation.hasText(offence_zip);
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });


        finish_button = (Button) findViewById(R.id.button_finish);
        finish_button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                /*
                Validation class will check the error and display the error on respective fields
                but it won't resist the form submission, so we need to check again before submit
                 */
                if ( checkValidation () )
                    submitForm();
                else
                    Toast.makeText(EditActivity.this, "Form contains error", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void submitForm() {
        // Submit your form here. your form is valid
        Toast.makeText(this, "Next ...", Toast.LENGTH_LONG).show();
        setResult(RESULT_OK, getIntent());
        finish();

    }

    private boolean checkValidation() {
        boolean ret = true;
        boolean ret_two = true;

        if (!Validation.hasText(offence_street)
                || !Validation.hasText(offence_zip)
                && !Validation.hasText(offence_city)
                || !Validation.hasText(offence_state))

            ret = false;
        return ret;

    }


}
