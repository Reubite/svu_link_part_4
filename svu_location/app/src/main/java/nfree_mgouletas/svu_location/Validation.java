package nfree_mgouletas.svu_location;

import android.widget.EditText;

import java.util.regex.Pattern;

/**
 * Created by rajayi on 12/8/16.
 */

public class Validation {


    private static final String VICTIM_INJURY_REGEX = "required";
    private static final String REQUIRED_MSG = "required";

    public static boolean isValid(EditText editText, String regex, String errMessage, boolean required){
        String txt = editText.getText().toString();
        editText.setError(null);

        if ( required && !hasText(editText))
            return false;

        if ( required && !Pattern.matches(regex, txt)){
            editText.setError(errMessage);
            return false;
        }
        return true;
    }

    public static boolean hasText(EditText editText) {

        String text = editText.getText().toString().trim();
        editText.setError(null);

        // length 0 means there is no text
        if (text.length() == 0) {
            editText.setError(REQUIRED_MSG);
            return false;
        }

        return true;
    }

}
